package com.example.bianca.pokeagenda;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.bianca.pokeagenda.Model.CustomJSONObjectRequest;
import com.example.bianca.pokeagenda.Model.CustomVolleyRequestQueue;
import com.example.bianca.pokeagenda.Model.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class DetalhesActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener {

    public static final String REQUEST_TAG = "UserAutentication";
    private RequestQueue mRequestQueue;
    private TextView mText;
    private String idPokemon;
    private String nomeTreinador;
    VolleyHelper volleyHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);
        try {
            volleyHelper = new VolleyHelper(this.getApplicationContext());
            JSONObject json = new JSONObject(getIntent().getStringExtra("pokemon"));
            ImageView imageView = findViewById(R.id.img_detalhes);
            TextView textNome = findViewById(R.id.nome_detalhes);
            TextView textEspecie = findViewById(R.id.especie_detalhes);
            TextView textPeso = findViewById(R.id.peso_detalhes);
            TextView textAltura = findViewById(R.id.altura_detalhes);
            TextView textTreinador = findViewById(R.id.treinador_detalhes);

            byte[]imageBytes = Base64.decode(json.get("photo").toString(), Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);//InputStream is = new ByteArrayInputStream(img.get(position).getBytes());
            imageView.setImageBitmap(decodedImage);

            textNome.setText(json.get("name").toString());
            textEspecie.setText(json.get("specie").toString());
            textPeso.setText(json.get("weight").toString());
            textAltura.setText(json.get("height").toString());
            textTreinador.setText(json.get("trainer").toString());

            Bundle params = getIntent().getExtras();

            nomeTreinador = params.getString("treinador");
            idPokemon = json.get("id").toString();
            //verifico o pokemon favorito
            volleyHelper.post("PokemonsOp?action=checkFav&nameTr="+nomeTreinador,new JSONObject(),this,this);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detalhes,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.menu_formulario_ok:
                String fav = "";
                CheckBox cbFavoritos = findViewById(R.id.cb_detalhes);
                if(cbFavoritos.isChecked()){
                    fav = "1";
                }else{
                    fav = "0";
                }
                volleyHelper.post("PokemonsOp?action=updateFav&fav="+fav+"&nameTr="+nomeTreinador+
                        "&idPok="+idPokemon,new JSONObject(),this,this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue!=null)
            mRequestQueue.cancelAll(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mText.setText("Resposta: "+error.getMessage());
    }

    @Override
    public void onResponse(Object response) {
        try{
            //
            String retorno = response.toString();
            if(retorno.contains("atualizou")){
                String op = ((JSONObject)response).getString("atualizou");
                if (op.equals("true")) {
                    Toast.makeText(this, "Atualizado com sucesso !", Toast.LENGTH_SHORT).show();
                }
                finish();
            }else if(retorno.contains("fav")){
                String op = ((JSONObject)response).getString("fav");
                CheckBox cb = findViewById(R.id.cb_detalhes);
                if(op != "" && idPokemon.equals(op)){
                    cb.setChecked(true);
                }else{
                    cb.setChecked(false);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
