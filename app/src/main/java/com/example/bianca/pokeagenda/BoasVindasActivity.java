package com.example.bianca.pokeagenda;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.bianca.pokeagenda.Model.CustomJSONObjectRequest;
import com.example.bianca.pokeagenda.Model.CustomVolleyRequestQueue;
import com.example.bianca.pokeagenda.Model.VolleyHelper;

import org.json.JSONObject;

public class BoasVindasActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener {
    TextView recebeNome;
    public static final String REQUEST_TAG = "UserAutentication";
    private RequestQueue mRequestQueue;
    private TextView mText;
    private String texto;
    VolleyHelper volleyHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boas_vindas);
        volleyHelper = new VolleyHelper(this.getApplicationContext());
        Intent pegaNomeBoasVindas = getIntent();
        if (pegaNomeBoasVindas != null) {
            Bundle params = pegaNomeBoasVindas.getExtras();
            if (params != null) {
                texto = params.getString("texto");
                recebeNome = (TextView) findViewById(R.id.recebeNome);
                recebeNome.setText("Bem vindo(a) Treinador(a) " + texto);

            }
        }
    carregarImagem();
    }

    public void carregarImagem(){
        volleyHelper.post("PokemonsOp?action=loadMainImg&nameTr="+texto,new JSONObject(),this,this);
    }
    //Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_boas_vindas,menu);
        return super.onCreateOptionsMenu(menu);
    }
    //Menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cadastrar:
                Intent cadastro = new Intent(BoasVindasActivity.this,FormularioCadastroActivity.class);
                Bundle paramsCadastro = new Bundle();
                Bundle paramsTr = getIntent().getExtras();
                paramsCadastro.putString("treinador",paramsTr.getString("texto"));
                cadastro.putExtras(paramsCadastro);
                startActivityForResult(cadastro,1);
                //finish();//retorna para a tela anterior
                break;

            case R.id.menu_consulta:
                Intent consulta = new Intent(BoasVindasActivity.this,FormularioConsultaActivity.class);
                Bundle params = new Bundle();
                Bundle params2 = getIntent().getExtras();
                params.putString("treinador",params2.getString("texto"));
                consulta.putExtras(params);
                startActivityForResult(consulta,1);
                //finish();
                break;

            case R.id.menu_pesquisa:
                Intent pesquisa = new Intent(BoasVindasActivity.this,FormularioPesquisaActivity.class);
                startActivity(pesquisa);
                break;

            case R.id.menu_sair:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue!=null)
            mRequestQueue.cancelAll(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mText.setText("Resposta: "+error.getMessage());
    }

    @Override
    public void onResponse(Object response) {
        try{
            String retorno = response.toString();
            if(retorno.contains("foto")){
                String op = ((JSONObject)response).getString("foto");
                if (op != "") {
                    byte[]imageBytes = Base64.decode(op, Base64.DEFAULT);
                    Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                    ImageView imageView = findViewById(R.id.imagemPokemonFavorito);
                    imageView.setImageBitmap(decodedImage);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            carregarImagem();
        }
    }
}
