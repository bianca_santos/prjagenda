package com.example.bianca.pokeagenda.Adapter;

import com.example.bianca.pokeagenda.Model.Pokemon;
import com.example.bianca.pokeagenda.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Walter on 16/06/2018.
 */

public class PokemonAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> name;
    private final ArrayList<String> species;
    private final ArrayList<String> img;

    //private final ByteArrayInputStream[] img;


    public PokemonAdapter(Activity context, ArrayList<String> name, ArrayList<String> species, ArrayList<String> img) {
        super(context,R.layout.list_item,name);
        this.context = context;
        this.name = name;
        this.species = species;
        this.img = img;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item,null,true);
        TextView txtTitle = rowView.findViewById(R.id.name);
        TextView txtDesc = rowView.findViewById(R.id.species);
        ImageView imageView = rowView.findViewById(R.id.img);
        txtTitle.setText(name.get(position));
        txtDesc.setText(species.get(position));

        byte[]imageBytes = Base64.decode(img.get(position), Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);//InputStream is = new ByteArrayInputStream(img.get(position).getBytes());

        //Bitmap bmp = BitmapFactory.decodeByteArray(decodedImage);

        imageView.setImageBitmap(decodedImage);

        return rowView;
    }
}
