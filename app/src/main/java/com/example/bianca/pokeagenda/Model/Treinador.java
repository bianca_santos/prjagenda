package com.example.bianca.pokeagenda.Model;

public class Treinador {

    private Integer id;
    private String nome;
    private String login;
    private String senha;
    private String favorito;

    public Treinador(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin(){return login;}

    public void setLogin(String login){this.login = login;}

    public String getSenha(){return senha;}

    public void setSenha(String senha){this.senha = senha;}

    public String getFavorito() {
        return favorito;
    }

    public void setFavorito(String favorito) {
        this.favorito = favorito;
    }

    public Integer getId() { return id;}

    public void setId(Integer id){this.id = id;}
}
