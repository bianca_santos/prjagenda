package com.example.bianca.pokeagenda.Model;

import java.io.Serializable;
import java.sql.Blob;

public class Pokemon implements Serializable {

    private Integer id;
    private String nome;
    private String especie;
    private double peso;
    private double altura;
    private Treinador treinador;
    private boolean favorito;
    private String caminhoFoto;


   public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) { this.peso = peso;}

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public Treinador getTreinador() {
        return treinador;
    }

    public void setTreinador(Treinador treinador) {
        this.treinador = treinador;
    }

    public boolean isFavorito() { return favorito; }

    public void setFavorito(boolean favorito) { this.favorito = favorito; }

    public Integer getId() {return id;}

    public void setId(Integer id){this.id = id;}

    public String getCaminhoFoto() { return caminhoFoto;}

    public void setCaminhoFoto(String caminhoFoto) { this.caminhoFoto = caminhoFoto;}

    @Override
    public String toString() {
        return getNome() + " - " + getEspecie();
    }
}
