package com.example.bianca.pokeagenda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bianca.pokeagenda.Model.CustomJSONObjectRequest;
import com.example.bianca.pokeagenda.Model.CustomVolleyRequestQueue;
import com.example.bianca.pokeagenda.Model.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

//dúvida: como escolher a senha que estamos digitando

public class LoginActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener {

    EditText login;
    String n;
    public static final String REQUEST_TAG = "UserAutentication";
    private RequestQueue mRequestQueue;
    private TextView mText;
    private Button bLogin;
    private EditText editLogin;
    private EditText editSenha;
    VolleyHelper volleyHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mText = findViewById(R.id.text_teste);
        bLogin = findViewById(R.id.btnLogin);
        editLogin = findViewById(R.id.login);
        editSenha = findViewById(R.id.senha);
        volleyHelper = new VolleyHelper(this.getApplicationContext());
    }

    public void login(View view){
        volleyHelper.post("UserValidator?login="+editLogin.getText().toString()+
        "&senha="+editSenha.getText().toString(),new JSONObject(),this,this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue!=null)
            mRequestQueue.cancelAll(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mText.setText("Resposta: "+error.getMessage());
    }

    @Override
    public void onResponse(Object response) {
        //mText.setText("Resposta: "+response);
        try{
             String retorno = ((JSONObject)response).getString("user");
             if(retorno.equals("invalid")){
                 editSenha.setError("Usuário e/ou senha inválidos");
             }
             else{
                 Intent boasVindas = new Intent(LoginActivity.this,BoasVindasActivity.class);
                 Bundle params = new Bundle();
                 params.putString("texto",retorno);
                 boasVindas.putExtras(params);
                 startActivity(boasVindas);
                 finish();
             }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
