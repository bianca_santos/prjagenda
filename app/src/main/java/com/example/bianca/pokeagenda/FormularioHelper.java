package com.example.bianca.pokeagenda;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.bianca.pokeagenda.Model.Pokemon;

//Classe responsável por fazer  mapeamento dos objetos x UI

public class FormularioHelper {
    private EditText campo_nome;
    private EditText campo_especie;
    private EditText campo_peso;
    private EditText campo_altura;
    private ImageView campo_foto;
    private CheckBox campo_favorito;

    private Pokemon pokemon;

    public FormularioHelper(FormularioCadastroActivity activity) {
        campo_nome = activity.findViewById(R.id.cadastro_nome);
        campo_especie = activity.findViewById(R.id.cadastro_especie);
        campo_peso = activity.findViewById(R.id.cadastro_peso);
        campo_altura = activity.findViewById(R.id.cadastro_altura);
        campo_foto = activity.findViewById(R.id.cadastro_foto);
        //campo_favorito = activity.findViewById(R.id.cadastro_favorito);//Não tenho certeza desse componente
        pokemon = new Pokemon();
    }

    //Método que instância um objeto no formulário cadastro activity, ou seja para cadas campo preenchido ele pega cria uma referência para o objeto ppkemon e salva
    public Pokemon getPokemon() {
        //Pokemon pokemon = new Pokemon();//constrói o pokémon e preenche cada campo
        pokemon.setNome(campo_nome.getText().toString());
        pokemon.setEspecie(campo_especie.getText().toString());
        pokemon.setPeso(Double.valueOf(campo_peso.getText().toString()));//não sei se está correto
        pokemon.setAltura(Double.valueOf(campo_altura.getText().toString()));//não sei se está correto
        pokemon.setCaminhoFoto((String) campo_foto.getTag());
        //pokemon.setFavorito(campo_favorito.isChecked());

        return pokemon;
    }

    public void preencheFormulario(Pokemon pokemon) {
        this.pokemon = pokemon;
        campo_nome.setText(pokemon.getNome());
        campo_especie.setText(pokemon.getEspecie());
        campo_peso.setText(Double.valueOf(pokemon.getPeso()).toString());
        campo_altura.setText(Double.valueOf(pokemon.getAltura()).toString());
        carregaImagem(pokemon.getCaminhoFoto());
        //campo_favorito.setText(pokemon.isFavorito());
    }

    public void carregaImagem(String caminhoFoto) {
        if (caminhoFoto != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            campo_foto.setImageBitmap(bitmapReduzido);
            campo_foto.setScaleType(ImageView.ScaleType.FIT_XY);
            campo_foto.setTag(caminhoFoto);
        }
    }
}
