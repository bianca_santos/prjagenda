package com.example.bianca.pokeagenda.Model;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by Walter on 18/06/2018.
 */

public class VolleyHelper {

    private final Context mContext;
    private final RequestQueue mRequestQueue;
    public static final String REQUEST_TAG = "UserAutentication";
    //private final ImageLoader mImageLoader;
    private final String mBaseUrl;

    public VolleyHelper(Context c){
        mContext = c;
        mRequestQueue = Volley.newRequestQueue(mContext);
        mBaseUrl = "http://192.168.25.100:8084/UserAutenticator/";
        //mImageLoader = new ImageLoader(mRequestQueue, new BitmapLruCache());
    }

    private String constructUrl(String method){
        return mBaseUrl + "/" + method;
    }

    /*public ImageLoader getImageLoader(){
        return mImageLoader;
    }*/

    public void get(String method, JSONObject jsonObject,
                    Listener<JSONObject> listener, ErrorListener errorListener){

        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Method.GET, constructUrl(method),jsonObject,
                listener,errorListener);
        jsonRequest.setTag(REQUEST_TAG);
        mRequestQueue.add(jsonRequest);
    }

    public void put(String method, JSONObject jsonObject,
                    Listener<JSONObject> listener, ErrorListener errorListener){

        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Method.PUT, constructUrl(method),jsonObject,
                listener,errorListener);
        jsonRequest.setTag(REQUEST_TAG);
        mRequestQueue.add(jsonRequest);
    }

    public void post(String method, JSONObject jsonObject,
                     Listener<JSONObject> listener, ErrorListener errorListener){
        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method.POST, constructUrl(method),jsonObject,
                listener,errorListener);
        jsonRequest.setTag(REQUEST_TAG);
        mRequestQueue.add(jsonRequest);

    }

    public void delete(String method, JSONObject jsonObject,
                       Listener<JSONObject> listener, ErrorListener errorListener){

        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Method.DELETE, constructUrl(method),jsonObject,
                listener,errorListener);
        jsonRequest.setTag(REQUEST_TAG);
        mRequestQueue.add(jsonRequest);
    }


}
