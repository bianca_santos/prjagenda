package com.example.bianca.pokeagenda;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.bianca.pokeagenda.Adapter.PokemonAdapter;
import com.example.bianca.pokeagenda.Dao.PokemonDAO;
import com.example.bianca.pokeagenda.Model.CustomJSONObjectRequest;
import com.example.bianca.pokeagenda.Model.CustomVolleyRequestQueue;
import com.example.bianca.pokeagenda.Model.HttpHandler;
import com.example.bianca.pokeagenda.Model.Pokemon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FormularioConsultaActivity extends AppCompatActivity{

    private ProgressDialog pDialog;
    private ListView lv;
    private String url = "http://192.168.25.100:8084/UserAutenticator/GetPokemons";

    ArrayList<String> pokeList;
    ArrayList<String> speciesList;
    ArrayList<String> imageList;
    ArrayList<String> heightList;
    ArrayList<String> weightList;
    ArrayList<String> idList;
    ArrayList<String> trainerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_consulta);

        pokeList = new ArrayList<>();
        imageList = new ArrayList<>();
        speciesList = new ArrayList<>();
        heightList = new ArrayList<>();
        weightList = new ArrayList<>();
        idList = new ArrayList<>();
        trainerList = new ArrayList<>();

        lv = findViewById(R.id.listaPokemons);
        new GetPokemons().execute();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //crio um obj para passar por parametro;
                try {
                    JSONObject jsonP = new JSONObject();
                    jsonP.put("id",idList.get(position));
                    jsonP.put("name",pokeList.get(position));
                    jsonP.put("photo",imageList.get(position));
                    jsonP.put("specie",speciesList.get(position));
                    jsonP.put("weight",weightList.get(position));
                    jsonP.put("height",heightList.get(position));
                    jsonP.put("trainer",trainerList.get(position));
                    //---------------irei abrir a activity de detalhes--------//
                    Intent it = new Intent(FormularioConsultaActivity.this,DetalhesActivity.class);
                    it.putExtra("pokemon",jsonP.toString());
                    Bundle params = getIntent().getExtras();
                    it.putExtras(params);
                    startActivity(it);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        //Para edição de um item
        /*listaPokemons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lista, View item, int position, long id) {
                Pokemon pokemon = (Pokemon) listaPokemons.getItemAtPosition(position);
                Intent irFormularioCadastro = new Intent(FormularioConsultaActivity.this,FormularioCadastroActivity.class);
                irFormularioCadastro.putExtra("dados",pokemon);
                startActivity(irFormularioCadastro);
            }
        });


        registerForContextMenu(listaPokemons);//cria o contexto do delete na tela consulta*/
    }

    public class GetPokemons extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pDialog = new ProgressDialog(FormularioConsultaActivity.this);
            pDialog.setMessage("Aguarde ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.makeServiceCall(url);
            if(jsonStr != null){
                try{
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    JSONArray pokemons = jsonObject.getJSONArray("pokemons");

                    for (int i=0; i<pokemons.length();i++){
                        JSONObject c = pokemons.getJSONObject(i);

                        idList.add(c.getString("id"));
                        pokeList.add(c.getString("nome"));
                        imageList.add(c.getString("foto2"));
                        speciesList.add(c.getString("especie"));
                        weightList.add(c.getString("peso"));
                        heightList.add(c.getString("altura"));
                        trainerList.add(c.getString("treinador"));
                    }
                }catch (final JSONException e){
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Json parsing error: "+
                                    e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "couldn't get JSON from server",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);

            if(pDialog.isShowing())
                pDialog.dismiss();

            PokemonAdapter adapter =  new PokemonAdapter(FormularioConsultaActivity.this,pokeList,speciesList,imageList);
            lv.setAdapter(adapter);
        }
    }


    /*//método para excluir
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
       //Excluir item
       MenuItem deletar = menu.add("Deletar");
       deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
           @Override
           public boolean onMenuItemClick(MenuItem item) {
               AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
               Pokemon pokemon = (Pokemon)listaPokemons.getItemAtPosition(info.position);

               PokemonDAO dao = new PokemonDAO(FormularioConsultaActivity.this);
               dao.deleta(pokemon);
               dao.close();
               carregaLista();

               Toast.makeText(FormularioConsultaActivity.this, "Deletar o Pokémon " + pokemon.getNome(),Toast.LENGTH_SHORT).show();

               return false;
           }
       });

    }*/
}
