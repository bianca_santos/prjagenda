package com.example.bianca.pokeagenda.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.widget.Switch;

import com.example.bianca.pokeagenda.Model.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonDAO extends SQLiteOpenHelper {

    public PokemonDAO(Context context) {
        super(context, "pokeserver", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //se o banco de dados não existir esse método é chamado
        //Falta fazer o relacionamento da tabela treinador com o pokemon
        String sql = "CREATE TABLE Pokemon(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                                           " nome TEXT NOT NULL," +
                                           "especie TEXT NOT NULL, " +
                                           "peso REAL NOT NULL, " +
                                           "altura REAL NOT NULL," +
                                           "caminhoFoto TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "";
        switch (oldVersion) {
            case 1:
                sql = "ALTER TABLE Pokemon ADD COLUMN caminhoFoto TEXT";
                db.execSQL(sql);//Versão 2
        }
    }

    public void insere(Pokemon pokemon) {
        //Falta implementar o treinador
       SQLiteDatabase db = getWritableDatabase();//referência para escrever no banco

        ContentValues dados = getDadosPokemon(pokemon);

        db.insert("Pokemon", null,dados);
    }

    @NonNull
    private ContentValues getDadosPokemon(Pokemon pokemon) {
        ContentValues dados = new ContentValues();
        dados.put("nome",pokemon.getNome());
        dados.put("especie",pokemon.getEspecie());
        dados.put("peso", pokemon.getPeso());
        dados.put("altura",pokemon.getAltura());
        dados.put("caminhoFoto",pokemon.getCaminhoFoto());
        //dados.put("favorito", pokemon.isFavorito());
        return dados;
    }

    public List<Pokemon> buscaPokemon() {
        String sql = "SELECT * FROM Pokemon;";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);//faz a busca e devolve paa um cursor
        List<Pokemon> pokemons = new ArrayList<Pokemon>();

        while (c.moveToNext()) {

            Pokemon pokemon = new Pokemon();
            pokemon.setId(c.getInt(c.getColumnIndex("id")));
            pokemon.setNome(c.getString(c.getColumnIndex("nome")));
            pokemon.setEspecie(c.getString(c.getColumnIndex("especie")));
            pokemon.setPeso(c.getDouble(c.getColumnIndex("peso")));
            pokemon.setAltura(c.getDouble(c.getColumnIndex("altura")));
            pokemon.setCaminhoFoto(c.getString(c.getColumnIndex("caminhoFoto")));
            //inseri favorito
            pokemons.add(pokemon);//guarda o pokémon na lista
        }
        c.close();
        return pokemons;
    }

    public void deleta(Pokemon pokemon) {
        SQLiteDatabase db = getWritableDatabase();

        String[] params = {pokemon.getId().toString()};
        db.delete("Pokemon","id = ?",params);
    }

    public void altera(Pokemon pokemon) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = getDadosPokemon(pokemon);

        String[] params = {pokemon.getId().toString()};
        db.update("Pokemon",dados,"id = ?",params);

    }
}
