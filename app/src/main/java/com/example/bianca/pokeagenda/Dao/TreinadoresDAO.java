package com.example.bianca.pokeagenda.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bianca.pokeagenda.Model.Pokemon;
import com.example.bianca.pokeagenda.Model.Treinador;

import java.util.ArrayList;
import java.util.List;

public class TreinadoresDAO extends SQLiteOpenHelper {

    public TreinadoresDAO(Context context) {
        super(context, "pokeserver", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //se o banco de dados não existir esse método é chamado
        String sql = "CREATE TABLE treinadores(id INT PRIMARY KEY AUTOINCREMENT, nome TEXT NOT NULL, login TEXT NOT NULL, senha TEXT NOT NULL);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Se banco de dados existir esse método é chamado
        String sql = "DROP TABLE IF EXISTS treinadores";
        db.execSQL(sql);
        onCreate(db);
    }
}
