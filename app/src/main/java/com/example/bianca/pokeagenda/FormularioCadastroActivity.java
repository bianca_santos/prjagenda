package com.example.bianca.pokeagenda;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.bianca.pokeagenda.Adapter.PokemonAdapter;
import com.example.bianca.pokeagenda.Dao.PokemonDAO;
import com.example.bianca.pokeagenda.Model.HttpHandler;
import com.example.bianca.pokeagenda.Model.Pokemon;
import com.example.bianca.pokeagenda.Model.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;

//dúvida: Como retornar da tela de consulta para a tela de boas vindas?

public class FormularioCadastroActivity extends AppCompatActivity {

    public static final int CODIGO_CAMERA = 567;
    private FormularioHelper helper;
    VolleyHelper volleyHelper;
    private RequestQueue mRequestQueue;
    private TextView mText;
    public static final String REQUEST_TAG = "PokemonsOp";
    private String url = "http://192.168.25.100:8084/UserAutenticator/PokemonsOp?action=insertPokemon";
    private ProgressDialog pDialog;
    private String params="";
    private String userLogado = "";
    private String retorno = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cadastro);
        helper = new FormularioHelper(this);
        Bundle params = getIntent().getExtras();

        userLogado = params.getString("treinador");
        //início do código para comportamento do botão câmera
        Button botaoFoto = (Button)findViewById(R.id.cadastro_botao_foto);
        botaoFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//ação de capturar imagem
                if(intentCamera.resolveActivity(getPackageManager()) != null) {
                    //caminhoFoto = getExternalFilesDir(null) +  "/" + System.currentTimeMillis() + ".jpg";
                    //File arquivoFoto = new File(caminhoFoto);
                    //intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivoFoto));
                    startActivityForResult(intentCamera, CODIGO_CAMERA);
                }

            }
        });
    }
    //abrindo a foto
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODIGO_CAMERA &&
                resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                ImageView mImageView = findViewById(R.id.cadastro_foto);
                mImageView.setImageBitmap(imageBitmap);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cadastro,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override//método que salva o  formulário
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cadastrar:
                ImageView imagem = findViewById(R.id.cadastro_foto);

                BitmapDrawable drawable = (BitmapDrawable) imagem.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                byte[] byteFormat = stream.toByteArray();
                // get the base 64 string
                String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);


                TextView textNome = findViewById(R.id.cadastro_nome);
                TextView textEspecie = findViewById(R.id.cadastro_especie);
                TextView textPeso = findViewById(R.id.cadastro_peso);
                TextView textAltura = findViewById(R.id.cadastro_altura);
                CheckBox cbFavorito = findViewById(R.id.cadastro_favorito);
                String fav = "0";
                if (cbFavorito.isChecked())
                    fav = "1";

                params = "&nome="+textNome.getText().toString()+"&especie="+textEspecie.getText().toString()+
                        "&peso="+textPeso.getText().toString()+"&altura="+textAltura.getText().toString()+
                        "&favorito="+fav+"&imagem="+imgString+"&userCadastro="+userLogado;

                new InsertPokemons().execute();
                //cadastro o pokemon
                 break;
        }
        return super.onOptionsItemSelected(item);
    }

    public class InsertPokemons extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pDialog = new ProgressDialog(FormularioCadastroActivity.this);
            pDialog.setMessage("Aguarde ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.makeServiceCall(url+params);
            if(jsonStr != null){
                try{
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    retorno = jsonObject.getString("insert");
                    /*JSONArray pokemons = jsonObject.getJSONArray("pokemons");

                    for (int i=0; i<pokemons.length();i++){
                        JSONObject c = pokemons.getJSONObject(i);

                        idList.add(c.getString("id"));
                        pokeList.add(c.getString("nome"));
                        imageList.add(c.getString("foto2"));
                        speciesList.add(c.getString("especie"));
                        weightList.add(c.getString("peso"));
                        heightList.add(c.getString("altura"));
                        trainerList.add(c.getString("treinador"));
                    }*/
                }catch (final Exception e){
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Json parsing error: "+
                                    e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "couldn't get JSON from server",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);

            if(pDialog.isShowing())
                pDialog.dismiss();

            if(retorno == "OK"){
                Toast.makeText(FormularioCadastroActivity.this, "Cadastrado com sucesso !", Toast.LENGTH_LONG).show();
                finish();
            }else if (retorno =="idExists"){
                Toast.makeText(FormularioCadastroActivity.this, "Pokemon com mesmo nome já existente !", Toast.LENGTH_LONG).show();
            }else if(retorno == "Error"){
                Toast.makeText(FormularioCadastroActivity.this, "Erro ao cadastrar !", Toast.LENGTH_SHORT).show();
                finish();
            }
            //Toast.makeText(FormularioCadastroActivity.this, result.toString(), Toast.LENGTH_SHORT).show();
            //aki digo que deu certo ou errado !

        }
    }

}
